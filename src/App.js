import { Container } from "@mui/material";
import { BrowserRouter } from "react-router-dom";
import AppToolbar from "./components/toolbar/AppToolbar";
import AppRoutes from "./routes/AppRoutes";

const App = () => {

    return (
        <BrowserRouter>
            <AppToolbar />
            <Container>
                <AppRoutes />
            </Container>
        </BrowserRouter>
    )

}
export default App;
