import { AppBar, Button, Toolbar, Typography } from "@mui/material"
import { Link } from "react-router-dom"

const AppToolbar = () => {
    return (
        <AppBar position="sticky">
            <Toolbar>
                <Typography variant="h6" flexGrow={1}>New York Times Books</Typography>
                <Button component={Link} to="/lists" color="inherit">Home</Button>
            </Toolbar>
        </AppBar>
    )
}

export default AppToolbar