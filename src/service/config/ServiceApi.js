import axios from "axios"
import { API_KEY, URL_BASE } from "./ServiceConfig"

const buildServiceApi = () => {
    let api = axios.create({
        baseURL: URL_BASE,
        params: {
            'api-key': API_KEY
        },
    })

    return api
}

const serviceApi = buildServiceApi()

export default serviceApi