import serviceApi from "../config/ServiceApi"
import { WS_LIST_TYPES, WS_LIST_BOOKS } from "../config/ServiceConfig"

const getListTypes = async () => {
    let response = await serviceApi.get(WS_LIST_TYPES)
    let result = response.data.results.map(
        item => {
            return {
                id: item.list_name_encoded,
                description: item.list_name,
                lastUpdate: item.newest_published_date
            }
        }
    )
    
    return result
}

const getList = async (listId) => {
    let response = await serviceApi.get(`${WS_LIST_BOOKS}/${listId}.json`)
    let results = response.data.results
    
    let listBooks = results.books.map(
        book => {
            return {
                rank: book.rank,
                publisher: book.publisher,
                description: book.description,
                title: book.title,
                author: book.author,
                image: book.book_image
            }
        }
    )

    return {
        listName: results.list_name,
        books: listBooks
    }
}

const bookService = { getListTypes, getList }

export default bookService