import { Route, Switch } from "react-router"
import BooksPage from "../pages/books/BooksPage"
import ListsPage from "../pages/list/ListsPage"

const AppRoutes = () => {
    return (
        <Switch>
            <Route path="/lists" component={ListsPage} />
            <Route path="/books/:listId" component={BooksPage} />
            <Route path="/" exact component={ListsPage} />
        </Switch>
    )
}

export default AppRoutes