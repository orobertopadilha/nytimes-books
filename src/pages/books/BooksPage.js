import { Grid, Typography } from '@mui/material'
import { useEffect, useState } from 'react'
import { useParams } from 'react-router'
import bookService from '../../service/books/BookService'
import BookItem from './BookItem'

const BooksPage = () => {
    const [books, setBooks] = useState([])
    const [listName, setListName] = useState('')

    const { listId } = useParams()

    useEffect(() => {

        const loadBooks = async () => {
            let result = await bookService.getList(listId)
            
            setBooks(result.books)
            setListName(result.listName)
        }

        loadBooks()
    }, [ listId ])

    return (
        <>
            <Typography variant="h5">
                {listName}
            </Typography>
            <Grid container spacing={2} marginTop={2}>
                {
                    books.map( book => 
                        <BookItem key={book.rank} book={book} />
                    )
                }
            </Grid>
        </>
    )
}

export default BooksPage