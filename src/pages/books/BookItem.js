import { Grid, Paper, Stack, Typography } from '@mui/material'

const BookItem = ({ book }) => {
    return (
        <Grid item md={4} sm={6} xs={12}>
            <Stack padding={2} borderRadius={2} component={Paper} height={350}>
                <img src={book.image} style={{ objectFit: 'cover', height: "200px" }} />

                <Typography fontWeight="bold" variant="subtitle1">
                    {book.rank}. {book.title}
                </Typography>
                <Typography variant="body2">
                    {book.author}
                </Typography>
                <Typography variant="body2" marginTop={1}>
                    {book.description}
                </Typography>
            </Stack>
        </Grid>
    )
}

export default BookItem