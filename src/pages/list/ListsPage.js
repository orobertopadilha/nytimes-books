import { Grid } from "@mui/material"
import { useEffect, useState } from "react"
import { useHistory } from "react-router"
import bookService from '../../service/books/BookService'
import ListTypeItem from "./ListTypeItem"

const ListsPage = () => {
    const [listTypes, setListTypes] = useState([])
    const history = useHistory()

    useEffect(() => {
        const loadTypes = async () => {
            let result = await bookService.getListTypes()
            setListTypes(result)
        }

        loadTypes()
    }, [])

    const onItemClick = (listId) => {
        history.push(`/books/${listId}`)
    }

    return (
        <Grid container spacing={2}>
          {
              listTypes.map( listType => 
                    <ListTypeItem key={listType.id} listType={listType} onItemClick={onItemClick} />
              )
          }  
        </Grid>
    )
}

export default ListsPage