import { Grid, Paper, Stack, Typography } from "@mui/material"
import moment from "moment"

const ListTypeItem = ({ listType, onItemClick }) => {
    return (
        <Grid item md={3} sm={4} xs={6} onClick={() => onItemClick(listType.id)}>
            <Stack padding={2} borderRadius={2} component={Paper} height={96}>
                <Typography fontWeight="bold" variant="subtitle1" flexGrow={1}>
                    {listType.description}
                </Typography>
                <Typography variant="body2">
                    Atualizado em {moment(listType.lastUpdate).format('DD/MM/YYYY')}
                </Typography>                    
            </Stack>
        </Grid>
    )
}

export default ListTypeItem